# -*- encoding: utf-8 -*-
##############################################################################
#    
#    ODOO, Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2014 Sprintit Ltd (<http://sprintit.fi>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

{
    'name': 'Finland - Accounting',
    'version': '1.0',
    'author': 'Sprintit Ltd',
    'website': 'http://www.sprintit.fi',
    'category': 'Localization/Account Charts',
    'depends': ['account_accountant'],
    'description': """
Open Accounting Chart for Finland.
============================================================================================================= 
Based on the Template from http://www.tilit.info. Please keep in mind that you should review and adapt it with your Accountant, before using it in a live Environment.
""",
    'demo': [],
    'data': ['account_tax_code.xml',
             'account_chart.xml',
             'account_tax.xml',
             'l10n_chart_fi_wizard.xml'],
    'auto_install': False,
    'installable': True
}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
